# TOS HA Kubernetes Cluster

# Configuration d'un Load Balancer HAProxy sous Debian 12

Ce guide vous montrera comment configurer un Load Balancer HAProxy sous Debian 12 avec 1 processeur et 512 Mo de RAM.

## Étape 1 : Installation de HAProxy

Installez HAProxy en utilisant apt-get :

```shell
sudo apt-get install haproxy
```

## Étape 2 : Configuration de HAProxy

Utilisez l'éditeur de texte Nano pour ouvrir le fichier de configuration HAProxy :

```shell
sudo nano /etc/haproxy/haproxy.cfg
```

Ajoutez ou modifiez la configuration suivante :

```shell
global
  log /dev/log local0
  log /dev/log local1 notice
  chroot /var/lib/haproxy
  stats socket /run/haproxy/admin.sock mode 660 level admin
  stats timeout 30s
  user haproxy
  group haproxy
  daemon

# Default SSL material locations
ca-base /etc/ssl/certs
crt-base /etc/ssl/private

# Configuration des paramètres SSL
ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA>
ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets

defaults
  log global
  mode tcp
  option tcplog
  timeout connect 5000
  timeout client 50000
  timeout server 50000
  errorfile 400 /etc/haproxy/errors/400.http
  errorfile 403 /etc/haproxy/errors/403.http
  errorfile 408 /etc/haproxy/errors/408.http
  errorfile 500 /etc/haproxy/errors/500.http
  errorfile 502 /etc/haproxy/errors/502.http
  errorfile 503 /etc/haproxy/errors/503.http
  errorfile 504 /etc/haproxy/errors/504.http

frontend k8s-api
  bind *:6443
  mode tcp
  default_backend k8s-api-servers

backend k8s-api-servers
  balance roundrobin
  mode tcp
  server Control_Plan1 IP:6443 check
  server Control_Plan2 IP:6443 check
  server Control_Plan3 IP:6443 check
  # Ajoutez plus de serveurs Kubernetes (maîtres) au besoin
```

Enregistrez les modifications et fermez l'éditeur Nano.

execution de cette commande : 

```shell
sudo kubeadm init --control-plane-endpoint IPduHAProxy:6443 --upload-certs
```
```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Étape 3 : Redémarrage de HAProxy

Redirection à faire sur proxmox :

```shell
iptables -t nat -A PREROUTING -i vmbr0 -p tcp --dport 6443 -j DNAT --to IPduHAProxy:6443
iptables -t nat -A POSTROUTING -o vmbr0 -j MASQUERADE
```

Redémarrez le service HAProxy pour appliquer la nouvelle configuration :

```shell
sudo systemctl restart haproxy
```

Vous devriez avoir un retour de ce type : 

You can now join any number of the control-plane node running the following command on each as root:

```shell
  kubeadm IP:6443 --token mo9au0.9e7al7sgk9lr41yt \
        --discovery-token-ca-cert-hash sha256:certificat \
        --control-plane --certificate-key certificat
```
Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use
"kubeadm init phase upload-certs --upload-certs" to reload certs afterward.

Then you can join any number of worker nodes by running the following on each as root:

```shell
kubeadm join IP:6443 --token mo9au0.9e7a77777lr41yt \
        --discovery-token-ca-cert-hash sha256:certificat
```
Il ne vous resteras plus qu'a lancer la première ou la deuxième en fonction de votre ressource (control-plain ou Node). 

Votre Load Balancer HAProxy est maintenant configuré pour équilibrer la charge entre les serveurs Kubernetes maîtres sur le port 6443. Vous pouvez ajouter davantage de serveurs au backend "k8s-api-servers" au besoin.

